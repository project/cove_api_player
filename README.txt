CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Use
 * For More Information


INTRODUCTION
------------

This module provides a Drupal field type and field formatter for displaying
PBS programs, clips, and promotions within an embedded video player. It is
intended for PBS member stations and program producers who wish to embed
COVE videos on their websites, and will likely not be useful outside of the
PBS.org ecosystem.

Module development was sponsored by 4Site Studios (http://www.4sitestudios.com)
on behalf of WETA-TV/FM (http://www.weta.org) and "Washington Week"
(http://www.pbs.org/weta/washingtonweek/").


REQUIREMENTS
------------

This module requires the PBS Cove API module
(https://www.drupal.org/project/cove_api), as well as a COVE API key from
PBS.


INSTALLATION
------------

After installing the COVE API module and configuring your API key, install COVE
API Player as you would any other Drupal module. If you need help, see
http://drupal.org/documentation/install/modules-themes/modules-7.


USE
---

To use, simply add a field of the "PBS Video" type to your content type under
"Manage Fields," and set it to be displayed as "PBS Cove Player" under manage
display.


FOR MORE INFORMATION
--------------------

For more information about COVE and to request an API key, see
https://projects.pbs.org/confluence/display/coveapi/Welcome.
